Ylitse API README
=================

[Ylitse Project][], started by SOS Children's Villages Finland, aims at
reducing the need for intergenerational child protecting services by
supporting young parents with a foster care background.

Ylitse API is a secure RESTful HTTP API is an interface to and implementation
of a digital peer mentoring service, which is at the core of Ylitse Project.

[Ylitse Project]: https://www.sos-lapsikyla.fi/mita-me-teemme/kehittamistyo/ylitse-projekti/

Dependencies
------------

Ylitse API is written in Python 3.5, but it also depends on several tools and
libraries:

* [aiohttp][]
* [Gunicorn][]
* [pycodestyle][]
* [pydocstyle][]
* [Pylint][]
* [pyresttest][] + [python-future][]
* [yamllint][]

It is also recommended to have [make][] installed. To install all dependencies
in a virtual environment run:

    make env

[aiohttp]: http://aiohttp.readthedocs.io/
[Gunicorn]: http://gunicorn.org/
[make]: https://www.gnu.org/software/make/
[pycodestyle]: http://pycodestyle.pycqa.org/
[pydocstyle]: http://www.pydocstyle.org/
[Pylint]: https://www.pylint.org/
[pyresttest]: https://github.com/svanoort/pyresttest/
[python-future]: http://python-future.org/
[yamllint]: https://yamllint.readthedocs.io/

Usage
-----

If you are using a virtual environment, created with `make env` for example,
activate it first:

    source env/bin/activate

Run Ylitse API with a single-threaded standalone `aiohttp` server (use
`run-standalone` target to skip tests).

    make run

Or start a `gunicorn` server with multiple workers:

    make run-gunicorn

By default, the API will be available at localhost on port 8080. You can now
do a simple test:

    curl -w '\n' 127.0.0.1:8080/version

If you are serving a web client on a local development web server, remember
to set `CORS_PORT` variable in `Makefile` accordingly.

There is more targets in `Makefile` you might want to check out.

Development
-----------

Contributed code should comply with [PEP 8] Style Guide and [PEP 257] Docstring
Conventions. You can check your code by running (this will also run Pylint code
analyzer):

    make lint

Unit tests are written using standard `unittest` module and integration tests
using `pyresttest`.

To run all tests and measure coverage simply run:

    make test

[PEP 8]: https://www.python.org/dev/peps/pep-0008/
[PEP 257]: https://www.python.org/dev/peps/pep-0257/

Releasing
---------

To bump a version, edit `ylitse/version.py` and NEWS, where [ISO 8601][]
formatted dates should be used:

    0.1.1 (2017-12-10)

After a release bump the version again with `+git` suffix and start a new NEWS
entry:

    0.1.1+git (unreleased)

Always tag releases:

    git tag v$(python setup.py --version)
    git push --tags

[ISO 8601]: https://www.iso.org/iso-8601-date-and-time-format.html

Deployment
----------

TODO

License
-------

Ylitse API is offered under the MIT license.
