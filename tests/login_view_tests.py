"""Tests for /logout endpoint."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

import ylitse


class LoginViewTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    async def get_cookie(self):
        payload = 'username=admin&password=secret'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        r = await self.client.request(
            'POST',
            '/login',
            data=payload,
            headers=headers
        )
        cookie = r.headers['Set-Cookie']
        a = cookie
        a = a[a.find('"'):a.find(';')]
        a = a[1:len(a)-1]
        cookie = {'YLITSE': a}
        h = cookie['YLITSE']
        h = 'YLITSE=' + h
        header = {'Cookie': h}
        return header

    @unittest_run_loop
    async def test_logout(self):
        cookie = await self.get_cookie()

        resp_user = await self.client.request('GET', '/user', headers=cookie)
        user_json = await resp_user.json()
        self.assertSetEqual(
            {*user_json},
            {'username', 'id', 'role', 'password'}
        )

        resp = await self.client.request('GET', '/logout', headers=cookie)
        json = await resp.json()
        self.assertEqual(json, {'message': 'You were logged out.'})

    @unittest_run_loop
    async def test_login_fail(self):
        payload = 'username=admin&password=janteri'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        r = await self.client.request(
            'POST',
            '/login',
            data=payload,
            headers=headers
        )
        self.assertEqual(r.status, 401)
