"""Tests for /accounts endpoint."""
from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

patch('aiohttp_security.login_required', lambda f: f).start()
patch('aiohttp_security.has_permission', lambda *x, **y: lambda f: f).start()

import ylitse  # noqa, pylint: disable=wrong-import-position


class AccountsViewTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    async def get_cookie(self):
        payload = 'username=admin&password=secret'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        r = await self.client.request(
            'POST',
            '/login',
            data=payload,
            headers=headers
        )
        cookie = r.headers['Set-Cookie']
        a = cookie
        a = a[a.find('"'):a.find(';')]
        a = a[1:len(a)-1]
        cookie = {'YLITSE': a}
        h = cookie['YLITSE']
        h = 'YLITSE=' + h
        header = {'Cookie': h}
        return header

    @unittest_run_loop
    async def test_post_and_get(self):
        cookie = await self.get_cookie()
        send = {
            'role': 'mentor',
            'email': 'esko.janteri@email.com',
            'password': 'secret',
            'username': 'esko',
            'nickname': 'janteri',
            'gender': 'female',
            'birth_year': 1992,
            'area': 'Helsinki, Kontula',
            'languages': ['Finnish'],
            'story': 'There will be lot of text here',
            'skills': ['babysitting'],
            'phone': '1123123123',
            'communication_channels': 'f2f, phone, email',
            'favorites': 'none'
        }

        req1 = await self.client.request(
            'POST',
            '/accounts', json=send,
            # headers=cookie
        )

        resp1 = await req1.json()

        _id = resp1['id']

        route2 = '/accounts/' + _id
        req2 = await self.client.request('GET', route2, headers=cookie)
        resp2 = await req2.json()

        req3 = await self.client.request('GET', '/accounts', headers=cookie)
        resp3 = await req3.json()

        del send['password']

        self.assertEqual(req1.status, 201)
        self.assertEqual({*resp1}, {*send, 'id'})

        self.assertEqual(req2.status, 200)
        self.assertEqual({*resp2['resources']}, {*send, 'id'})

        self.assertEqual(req3.status, 200)

        self.assertEqual({**send, 'id': _id} in resp3['resources'], True)

    @unittest_run_loop
    async def test_accounts_end_point_post(self):
        old_accounts = ylitse.read_file_to_list(
            self.app['config']['data_file'])
        cookie = await self.get_cookie()

        send = {
            'role': 'mentor',
            'email': 'esko.janteri@email.com',
            'password': 'secret',
            'username': 'esko',
            'nickname': 'janteri',
            'gender': 'female',
            'birth_year': 1992,
            'area': 'Helsinki, Kontula',
            'languages': ["Finnish", "Russian"],
            'story': 'There will be lot of text here',
            'skills': ['babysitting'],
            'phone': '1123123123',
            'communication_channels': 'f2f, phone, email',
            'favorites': 'none'
        }

        req = await self.client.request(
            'POST', '/accounts',
            json=send,
            headers=cookie
        )

        resp = await req.json()
        new_accounts = ylitse.read_file_to_list(
            self.app['config']['data_file'])

        # del send['password']

        self.assertEqual(req.status, 201)
        self.assertEqual({*resp, 'password'}, {*send, 'id'})
        self.assertEqual(True, len(old_accounts) < len(new_accounts))

        send['phone'] = '030303030'
        send['id'] = resp['id']

        req2 = await self.client.request(
            'PUT', '/accounts/' + str(resp['id']), json=send
        )
        self.assertEqual(req2.status, 200)
        res2 = await req2.json()

        self.assertEqual(
            {k: res2[k] for k in res2 if k != 'password'},
            {k: send[k] for k in send if k != 'password'}
        )

        old_account_count = len(ylitse.read_file_to_list(
            self.app['config']['data_file']
        ))

        # self.assertEqual(old_account_count, 1)

        req = await self.client.request(
            'DELETE', '/accounts/' + str(resp['id'])
        )

        self.assertEqual(req.status, 204)

        new_account_count = len(ylitse.read_file_to_list(
            self.app['config']['data_file']
        ))

        self.assertEqual(old_account_count - 1, new_account_count)

    @unittest_run_loop
    async def test_accounts_end_point_post_fail(self):
        old_accounts = ylitse.read_file_to_list(
            self.app['config']['data_file'])
        send = {
            'role': 'Master of the universe',
            'email': 'esko.janteri@email.com',
            'password': 'secret',
            'username': 'esko',
            'nickname': 'janteri',
            'gender': 'female',
            'birth_year': '1992',
            'area': 'Helsinki, Kontula',
            'languages': 'fi, swe, ru',
            'story': 'There will be lot of text here',
            'skills': 'babysitting',
            'phone': '1123123123',
            'communication_channels': 'f2f, phone, email',
            'favorites': 'none'
        }

        await self.client.request('POST', '/accounts', json=send)

        new_accounts = ylitse.read_file_to_list(
            self.app['config']['data_file'])

        self.assertEqual(True, len(old_accounts) == len(new_accounts))

    @unittest_run_loop
    async def test_accounts_end_point_get(self):
        cookie = await self.get_cookie()
        req = await self.client.request('GET', '/accounts', headers=cookie)
        resp = await req.json()
        accounts_list = ylitse.read_file_to_list(
            self.app['config']['data_file'])
        accounts_list = [{k: r[k] for k in r.keys() if k != 'password'}
                         for r in accounts_list]

        self.assertEqual(req.status, 200)
        assert isinstance(resp["resources"], list)
        self.assertEqual(resp["resources"], accounts_list)

    @unittest_run_loop
    async def test_accounts_end_point_get_by_id(self):
        req = await self.client.request('GET', '/accounts/9999999')

        self.assertEqual(req.status, 404)

    @unittest_run_loop
    async def test_accounts_end_point_get_query_mentee(self):
        cookie = await self.get_cookie()
        req = await self.client.request(
            'GET',
            '/accounts?role=mentee',
            headers=cookie
        )

        self.assertEqual(req.status, 200)

    @unittest_run_loop
    async def test_accounts_end_point_get_query_mentee_unauth(self):
        # cookie = await self.get_cookie()
        resp = await self.client.request(
            'GET',
            '/accounts?role=mentee',
        )

        self.assertEqual(resp.status, 401)

    @unittest_run_loop
    async def test_accounts_end_point_get_query_lizard(self):
        cookie = await self.get_cookie()
        req = await self.client.request(
            'GET',
            '/accounts?role=lizard',
            headers=cookie
        )

        self.assertEqual(req.status, 400)
