"""Tests for /skills endpoint."""

import random
import string
from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

patch('aiohttp_security.login_required', lambda f: f).start()
patch('aiohttp_security.has_permission', lambda *x, **y: lambda f: f).start()
import ylitse  # noqa, pylint: disable=wrong-import-position


def random_string(n):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=n))


class SkillsViewTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    async def post(self, some):
        resp = await self.client.request('POST', '/skills', json=some)
        return resp

    async def post_random(self):
        sent = {'name': random_string(10)}
        resp = await self.post(sent)
        return (sent, resp)

    @unittest_run_loop
    async def test_post_and_get(self):
        (sent, resp) = await self.post_random()
        json = await resp.json()
        _id = json['id']
        self.assertEqual(resp.status, 201)
        self.assertEqual(json, {**sent, 'id': _id})

        resp2 = await self.client.request('GET', '/skills/' + _id)
        json2 = await resp2.json()
        self.assertEqual(resp2.status, 200)
        self.assertEqual(json, json2)

        resp3 = await self.client.request('GET', '/skills')
        json3 = await resp3.json()
        self.assertEqual(resp3.status, 200)
        self.assertEqual({**sent, 'id': _id} in json3['resources'], True)

    @unittest_run_loop
    async def test_post(self):

        old_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        (sent, resp) = await self.post_random()
        json = await resp.json()

        new_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        self.assertEqual(resp.status, 201)
        self.assertEqual({*json}, {*sent, 'id'})
        self.assertEqual(True, len(old_skills) < len(new_skills))

    @unittest_run_loop
    async def test_post_fail(self):
        old_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        resp = await self.post({'invalid': 'key'})

        new_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        self.assertEqual(resp.status, 400)
        # self.assertEqual({*resp}, {*send, 'id'})
        self.assertEqual(True, len(old_skills) == len(new_skills))

    @unittest_run_loop
    async def test_put(self):

        (_, resp_post) = await self.post_random()

        old_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        resp_json = await resp_post.json()
        _id = resp_json['id']

        mod_skill = {
            'id': _id,
            'name': random_string(10),
        }

        resp2 = await self.client.request(
            'PUT', '/skills/' + _id, json=mod_skill
        )
        json2 = await resp2.json()

        new_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )
        self.assertEqual(resp2.status, 200)
        self.assertEqual(json2, mod_skill)
        self.assertEqual(True, len(old_skills) == len(new_skills))
        self.assertEqual(True, mod_skill in new_skills)
        self.assertEqual(True, mod_skill not in old_skills)

    @unittest_run_loop
    async def test_put_fail(self):
        (_, resp_post) = await self.post_random()

        old_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        resp_json = await resp_post.json()
        _id = resp_json['id']

        mod_skill = {
            'id': "99999",
            'name': random_string(10),
        }

        resp2 = await self.client.request(
            'PUT', '/skills/' + _id, json=mod_skill
        )
        json2 = await resp2.json()

        new_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )
        self.assertEqual(resp2.status, 400)
        self.assertEqual(True, len(old_skills) == len(new_skills))
        self.assertEqual(True, mod_skill not in new_skills)
        self.assertEqual({*json2}, {'error'})

    @unittest_run_loop
    async def test_del(self):
        (_, resp_post) = await self.post_random()
        old_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )

        resp_json = await resp_post.json()
        _id = resp_json['id']
        resp2 = await self.client.request('DELETE', '/skills/' + _id)

        new_skills = ylitse.read_file_to_list(
            self.app['config']['data_file_skills']
        )
        self.assertEqual(resp2.status, 204)
        self.assertEqual(True, len(old_skills) > len(new_skills))

    @unittest_run_loop
    async def test_del_fail(self):
        req = await self.client.request('DELETE', '/skills/9999999')

        self.assertEqual(req.status, 404)

    @unittest_run_loop
    async def test_get_by_id_fail(self):
        req = await self.client.request('GET', '/skills/9999999')

        self.assertEqual(req.status, 404)
