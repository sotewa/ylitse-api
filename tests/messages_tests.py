"""Tests for /messages endpoint."""
from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

patch('aiohttp_security.login_required', lambda f: f).start()
patch('aiohttp_security.has_permission', lambda *x, **y: lambda f: f).start()

import ylitse  # noqa, pylint: disable=wrong-import-position


class AccountsViewTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    async def get_cookie(self, u, p):
        payload = 'username=' + u + '&password=' + p
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        r = await self.client.request(
            'POST',
            '/login',
            data=payload,
            headers=headers
        )
        cookie = r.headers['Set-Cookie']
        a = cookie
        a = a[a.find('"'):a.find(';')]
        a = a[1:len(a)-1]
        cookie = {'YLITSE': a}
        h = cookie['YLITSE']
        h = 'YLITSE=' + h
        header = {'Cookie': h}
        return header

    @unittest_run_loop
    async def test_post_and_get(self):  # noqa, pylint: disable=too-many-locals
        cookie = await self.get_cookie('admin', 'secret')
        send_seppo = {
            'role': 'mentor',
            'email': 'asdfesko.janteri@email.com',
            'password': 'secret',
            'username': 'seppo',
            'nickname': 'janteri',
            'gender': 'female',
            'birth_year': 1992,
            'area': 'Helsinki, Kontula',
            'languages': ['Finnish'],
            'story': 'There will be lot of text here',
            'skills': ['babysitting'],
            'phone': '1123123123',
            'communication_channels': 'f2f, phone, email',
            'favorites': 'none'
        }
        seppo_req = await self.client.request(
            'POST',
            '/accounts', json=send_seppo,
            headers=cookie
        )
        seppo_json = await seppo_req.json()

        send_anni = {
            'role': 'mentee',
            'email': 'anni.janteri@email.com',
            'password': 'secret',
            'username': 'anni',
            'nickname': 'anni',
            'favorites': []
        }
        anni_req = await self.client.request(
            'POST',
            '/accounts', json=send_anni,
            headers=cookie
        )
        anni_json = await anni_req.json()

        anni_cookie = await self.get_cookie('anni', 'secret')
        print('tests anni id ' + anni_json['id'])

        msg_from_anni_to_seppo = {
            "sender_id": anni_json['id'],
            "recipient_id": seppo_json['id'],
            "content": "Moi Esko!! MITA KUULUU?? :D XD LOL"
        }

        msg_send_r = await self.client.request(
            'POST',
            '/messages', json=msg_from_anni_to_seppo,
            headers=anni_cookie
        )
        self.assertEqual(msg_send_r.status, 201)

        msg_json = await msg_send_r.json()
        m = msg_from_anni_to_seppo
        self.assertEqual(m['content'], msg_json['content'])
        self.assertEqual(m['sender_id'], msg_json['sender_id'])
        self.assertEqual(m['recipient_id'], msg_json['recipient_id'])

        seppo_cookie = await self.get_cookie('seppo', 'secret')
        msg_get_seppo = await self.client.request(
            'GET',
            '/messages', json=msg_from_anni_to_seppo,
            headers=seppo_cookie
        )

        self.assertEqual(msg_get_seppo.status, 200)
        seppos_messages = await msg_get_seppo.json()
        self.assertEqual(msg_json in seppos_messages['resources'], True)

        some = next(iter(seppos_messages['resources']))
        id_val = some['id']
        self.assertEqual(False, some['opened'])

        seppo_sees = await self.client.request(
            'PUT',
            '/messages/' + id_val,
            headers=seppo_cookie
        )
        self.assertEqual(seppo_sees.status, 200)

        msg_get_seppo2 = await self.client.request(
            'GET',
            '/messages', json=msg_from_anni_to_seppo,
            headers=seppo_cookie
        )
        msgs2 = (await msg_get_seppo2.json())['resources']
        some2 = next(m for m in msgs2 if m['id'] == id_val)
        self.assertEqual(True, some2['opened'])
