from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

import ylitse


class MiddlewareTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    @unittest_run_loop
    async def test_error_404_handler(self):
        res = await self.client.request('GET', '/hep')

        self.assertEqual(res.status, 404)

    @unittest_run_loop
    async def test_original_handler(self):
        with patch('ylitse.middleware.ERROR_HANDLERS', {}):
            res = await self.client.request('GET', '/hep')

            self.assertEqual(res.status, 404)
