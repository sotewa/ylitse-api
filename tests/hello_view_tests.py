"""Tests for /hello endpoint."""

from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

patch('aiohttp_security.login_required', lambda f: f).start()
patch('aiohttp_security.has_permission', lambda *x, **y: lambda f: f).start()
import ylitse  # noqa, pylint: disable=wrong-import-position


class HelloViewTests(AioHTTPTestCase):
    async def get_application(self):
        return ylitse.create_app()

    @unittest_run_loop
    async def test_hello_endpoint_get_method(self):
        correct_resp = {'message': 'Hello world!'}
        req = await self.client.request('GET', '/hello')
        resp = await req.json()

        self.assertEqual(req.status, 200)
        self.assertEqual(resp, correct_resp)

    @unittest_run_loop
    async def test_hello_endpoint_put_method(self):
        correct_resp = {'message': 'Hello Maikkeli!'}
        req = await self.client.request(
            'PUT', '/hello', json={
                'target': 'Maikkeli'
            }
        )
        resp = await req.json()

        self.assertEqual(req.status, 200)
        self.assertEqual(resp, correct_resp)
