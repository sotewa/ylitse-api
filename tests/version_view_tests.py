from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

import ylitse


class VersionViewTests(AioHTTPTestCase):

    async def get_application(self):
        return ylitse.create_app()

    @unittest_run_loop
    async def test_included_versions(self):
        req = await self.client.request('GET', '/version')
        self.assertEqual(req.status, 200)

        versions = await req.json()
        self.assertEqual(list(versions.keys()), ['api'])
