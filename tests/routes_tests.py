import unittest

import aiohttp

import ylitse


class RoutesTest(unittest.TestCase):

    def test_added_resources(self):
        resources = {
            '/login',
            '/logout',
            '/user',
            '/version',
            '/hello',
            '/accounts',
            '/skills',
            '/messages',
        }
        app = ylitse.create_app()
        res = app.router.resources()
        pls = list(
            filter(lambda x: isinstance(x, aiohttp.web.PlainResource), res)
        )
        new_resources = set(map(lambda x: str(x.url_for()), pls))

        self.assertEqual(new_resources, resources)
