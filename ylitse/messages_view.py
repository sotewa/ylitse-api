"""messages module.

Contains `/messages` endpoint implementation.
"""
import time
from aiohttp import web
from aiohttp_security import login_required, authorized_userid

import ylitse


class MessagesView:
    """Endpoint methods."""

    def __init__(self, data_file):
        """Accounts view constructor."""
        self._data_file = data_file

    def write(self, some):
        """Write list of skills to file."""
        ylitse.write_list_to_file(some, self._data_file)

    def read(self):
        """Read skills."""
        return ylitse.read_file_to_list(self._data_file)

    @login_required
    async def get_messages(self, request):
        """Get messages."""
        user = request.app['accounts'].get(await authorized_userid(request))
        user_id = user['id']
        all_messages = self.read()
        user_messages = list(filter(
            lambda m: user_id in [m['sender_id'], m['recipient_id']],
            all_messages
        ))
        return web.json_response({'resources': user_messages})

    @login_required
    async def post_message(self, request):
        """Post a message."""
        user = request.app['accounts'].get(await authorized_userid(request))
        user_id = user['id']

        json = await request.json()
        json['sender_id'] = user_id
        json['id'] = str(time.time()).replace('.', '')
        json['timestamp'] = int(time.time())
        json['opened'] = False

        self.write([*self.read(), json])
        return web.json_response(json, status=201)

    @login_required
    async def put_message(self, request):
        """See a message."""
        id_val = request.match_info['id']

        all_messages = self.read()
        message = next((m for m in all_messages if m['id'] == id_val), None)
        if not message:
            raise web.HTTPNotFound
        message['opened'] = True
        self.write(all_messages)
        return web.json_response(message, status=200)
