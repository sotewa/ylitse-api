"""Version view module.

Contains `/version` endpoint implementation.
"""

from aiohttp import web

import ylitse


class VersionView:
    """Endpoint methods."""

    async def get_version(self, request):
        """Return component versions."""
        return web.json_response({'api': ylitse.__version__})
