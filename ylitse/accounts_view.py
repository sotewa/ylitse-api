"""accounts module.

Contains `/accounts` endpoint implementation.
"""

import json
import time

from aiohttp import web
from aiohttp_security import is_anonymous, has_permission, login_required, \
        authorized_userid, permits

import ylitse


def read_file_to_list(file_name):
    """Open file and read it to list."""
    contents = []

    try:
        with open(file_name, 'r') as file:
            contents = json.load(file)
    except ValueError:
        print(f'Invalid JSON in {file_name}')
    except FileNotFoundError:
        print(f'File {file_name} not found')

    return contents


def write_list_to_file(list_to_write, file_name):
    """Open file and write a list to it."""
    with open(file_name, 'w') as f:
        json.dump(list_to_write, f)


class AccountsView:
    """Endpoint methods."""

    def __init__(self, accounts, data_file):
        """Accounts view constructor."""
        self._accounts = accounts
        self._data_file = data_file

    async def get_accounts(self, request):
        """Return a list of all mentors."""
        resp = {'resources': [], 'errors': [], 'messages': []}
        status = 200

        q = request.query
        anonymous = await is_anonymous(request)

        # only mentor list is public
        if anonymous and q.get('role') != 'mentor':
            raise web.HTTPUnauthorized

        if not q:
            resp['resources'] = read_file_to_list(self._data_file)
            status = 200
        elif 'role' in q and q['role'] in ['admin', 'mentee', 'mentor']:
            resp['resources'] = [
                *filter(lambda x: x['role'] == q['role'],
                        read_file_to_list(self._data_file))
            ]
            status = 200
        else:
            resp['errors'] = ['Invalid query.']
            status = 400

        resp['resources'] = [{k: r[k] for k in r.keys() if k != 'password'}
                             for r in resp['resources']]

        return web.json_response(resp, status=status)

    @has_permission('admin')
    async def get_account_by_id(self, request):
        """Return account based on the id given."""
        resp = {'resources': [], 'errors': [], 'messages': []}
        status = 200

        _id = request.match_info['id']
        print(_id)
        account = list(
            filter(lambda x: x['id'] == str(_id),
                   read_file_to_list(self._data_file))
        )
        if account:
            resp['resources'] = {k: account[0][k] for k in account[0].keys()
                                 if k != 'password'}
            ret = web.json_response(resp, status=status)
        else:
            status = 404
            ret = web.Response(status=status)

        return ret

    # @has_permission('admin')
    async def post_account(self, request):
        """Add an account to db. Return the added account."""
        resp = {'resources': [], 'errors': [], 'messages': []}

        req_body = await request.json()

        new_account = {
            **req_body, 'id': str(time.time()).replace('.', '')
        }
        if ('role' in new_account
                and new_account['role'] in ['admin', 'mentee', 'mentor']):
            if new_account['role'] != 'mentee':
                if not await permits(request, 'admin'):
                    raise web.HTTPUnauthorized
            password = new_account['password']
            new_account['password'] = ylitse.hash_password(password)
            status = 201
            resp = dict(new_account)
            del resp['password']
            accounts = [*read_file_to_list(self._data_file), new_account]
            write_list_to_file(accounts, self._data_file)
            self._accounts[new_account['username']] = new_account
        else:
            status = 400
            resp['errors'] = 'Invalid role.'

        return web.json_response(resp, status=status)

    @login_required
    async def put_account(self, request):
        """Add an account to db. Return the added account."""
        id_val = request.match_info['id']
        accounts = read_file_to_list(self._data_file)
        target_account = next(
            iter(x for x in accounts if x['id'] == id_val),
            None
        )
        if not target_account:
            raise web.HTTPNotFound

        putter_dude = self._accounts.get(await authorized_userid(request))
        if not putter_dude:
            raise web.HTTPUnauthorized

        if target_account.get('id') != putter_dude.get('id'):
            if not await permits(request, 'admin'):
                raise web.HTTPUnauthorized

        request_json = await request.json()
        request_json['id'] = id_val
        if request_json.get('id') != target_account.get('id'):
            raise web.HTTPUnauthorized

        if 'password' not in request_json:
            request_json['password'] = target_account['password']

        accounts = [x if x['id'] != id_val else request_json for x in accounts]

        if request_json['password'] != target_account['password']:
            request_json['password'] = ylitse.hash_password(
                request_json['password']
            )

        self._accounts.pop(target_account['username'])
        self._accounts[request_json['username']] = request_json

        write_list_to_file(accounts, self._data_file)

        response = {**request_json, 'password': None}

        return web.json_response(
            response,
            status=200
        )

    @login_required
    async def delete_account(self, request):
        """Deletet an account."""
        id_val = request.match_info['id']

        accounts = read_file_to_list(self._data_file)
        target_account = next(
            iter(x for x in accounts if x['id'] == id_val),
            None
        )
        if not target_account:
            raise web.HTTPNotFound

        actor_dude = self._accounts.get(await authorized_userid(request))

        if target_account.get('id') != actor_dude.get('id'):
            if not await permits(request, 'admin'):
                raise web.HTTPUnauthorized

        accounts = [a for a in accounts if a['id'] != id_val]
        write_list_to_file(accounts, self._data_file)

        return web.Response(status=204)
