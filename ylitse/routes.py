"""Route module.

All endpoints are added to the router here.
"""

import ylitse


def setup_routes(app):
    """Add endpoints to a router."""
    login = ylitse.LoginView()
    app.router.add_post('/login', login.login)
    app.router.add_get('/logout', login.logout)
    app.router.add_get('/user', login.get_user)

    version = ylitse.VersionView()
    app.router.add_get('/version', version.get_version)

    hello = ylitse.HelloView()
    app.router.add_get('/hello', hello.get_hello)
    app.router.add_put('/hello', hello.put_hello)

    accounts = ylitse.AccountsView(
        app['accounts'],
        app['config'].get('data_file')
    )
    app.router.add_get('/accounts', accounts.get_accounts)
    app.router.add_get('/accounts/{id}', accounts.get_account_by_id)
    app.router.add_post('/accounts', accounts.post_account)
    app.router.add_put('/accounts/{id}', accounts.put_account)
    app.router.add_delete('/accounts/{id}', accounts.delete_account)

    skills = ylitse.SkillsView(
        app['skills'],
        app['config'].get('data_file_skills')
    )
    app.router.add_post('/skills', skills.post_skill)
    app.router.add_get('/skills', skills.get_skills)
    app.router.add_get('/skills/{id}', skills.get_skill_by_id)
    app.router.add_put('/skills/{id}', skills.put_skill)
    app.router.add_delete('/skills/{id}', skills.delete_skill)

    messages = ylitse.MessagesView(
        app['config'].get('data_file_messages')
    )
    app.router.add_post('/messages', messages.post_message)
    app.router.add_get('/messages', messages.get_messages)
    app.router.add_put('/messages/{id}', messages.put_message)
