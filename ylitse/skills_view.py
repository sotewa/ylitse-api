"""skills module.

Contains `/skills` endpoint implementation.
"""

import asyncio
import time
from aiohttp import web
from aiohttp_security import has_permission, login_required

from ylitse import write_list_to_file


class SkillsView:
    """Endpoint methods."""

    def __init__(self, skills, data_file):
        """Accounts view constructor."""
        self._skills = skills
        self._data_file = data_file

    def write(self):
        """Write list of skills to file."""
        write_list_to_file(self._skills, self._data_file)

    async def validate(self, validators) -> str:
        """Eval validators lazily and async."""
        for (f, e) in validators:
            await asyncio.sleep(0)
            if f():
                pass
            else:
                return e
        return ''

    async def find_error_post(self, skill) -> str:
        """Check short-circuit manner that skill is okay."""
        validators = []

        msg1 = 'Invalid json.'
        validators.append((lambda: len(skill) == 1, msg1))
        validators.append((lambda: {*skill.keys()} == {'name'}, msg1))
        validators.append((lambda: isinstance(skill['name'], str), msg1))

        msg2 = 'Skill already exists.'
        existing_skills = [*map(lambda x: x['name'], self._skills)]
        validators.append((lambda: skill['name'] not in existing_skills, msg2))

        return await self.validate(validators)

    async def find_error_put(self, skill, _id) -> str:
        """Check lazily that skill is okay."""
        validators = []

        msg1 = 'Invalid json.'
        validators.append((lambda: {*skill.keys()} == {'name', 'id'}, msg1))
        validators.append((lambda: isinstance(skill['name'], str), msg1))
        validators.append((lambda: isinstance(skill['id'], str), msg1))
        validators.append((lambda: skill['id'] == _id, msg1))

        msg2 = 'Skill already exists.'
        existing_names = [*map(lambda x: x['name'], self._skills)]
        validators.append((lambda: skill['name'] not in existing_names, msg2))

        msg3 = 'Not found.'
        existing_ids = [*map(lambda x: x['id'], self._skills)]
        validators.append((lambda: skill['id'] in existing_ids, msg3))

        return await self.validate(validators)

    @login_required
    async def get_skills(self, request):
        """Return a list of all self._skills."""
        resp = {'resources': [], 'errors': [], 'messages': []}
        resp['resources'] = self._skills
        return web.json_response(resp, status=200)

    @login_required
    async def get_skill_by_id(self, request):
        """Return skill based on the given id."""
        _id = str(request.match_info['id'])
        skill = next(filter(lambda x: x['id'] == _id, self._skills), None)
        if skill:
            ret = web.json_response(skill, status=200)
        else:
            ret = web.Response(status=404)

        return ret

    @has_permission('admin')
    async def post_skill(self, request):
        """Add a skill to db. Echo the skill back."""
        req_body = await request.json()
        error_msg = await self.find_error_post(req_body)
        if not error_msg:
            new_id = str(time.time()).replace('.', '')
            new_skill = {**req_body, 'id': new_id}
            self._skills.append(new_skill)
            self.write()
            ret = web.json_response(new_skill, status=201)
        else:
            resp = {'error': error_msg}
            ret = web.json_response(resp, status=400)

        return ret

    @has_permission('admin')
    async def put_skill(self, request):
        """Modify a skill in db. Echo the skill back."""
        _id = str(request.match_info['id'])
        req_body = await request.json()
        error_msg = await self.find_error_put(req_body, _id)
        if not error_msg:
            self._skills = [
                *filter(lambda x: x['id'] != req_body['id'], self._skills),
                req_body
            ]
            self.write()
            ret = web.json_response(req_body, status=200)
        else:
            ret = web.json_response({'error': error_msg}, status=400)

        return ret

    @has_permission('admin')
    async def delete_skill(self, request):
        """Delete a skill in db."""
        _id = str(request.match_info['id'])
        ids = map(lambda x: x['id'], self._skills)
        if _id in ids:
            self._skills = [
                *filter(lambda x: x['id'] != _id, self._skills)
            ]
            self.write()
            ret = web.Response(status=204)
        else:
            ret = web.Response(status=404)

        return ret
