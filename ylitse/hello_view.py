"""Hello world module.

Contains `/hello` endpoint implementation.
"""

from aiohttp import web
from aiohttp_security import login_required


class HelloView:
    """Endpoint methods."""

    @login_required
    async def get_hello(self, request):
        """Return Hello world."""
        return web.json_response({'message': 'Hello world!'})

    @login_required
    async def put_hello(self, request):
        """Return Hello {someones name}."""
        req_body = await request.json()
        return web.json_response(
            {'message': 'Hello ' + req_body.get('target') + '!'})
