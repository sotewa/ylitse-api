"""Login module.

Contains `/login` and `/logout` endpoint implementation.
"""

from aiohttp import web
from aiohttp_security import \
    login_required, remember, forget, authorized_userid

import ylitse


class LoginView:
    """Endpoint methods."""

    async def login(self, request):
        """Process login request."""
        form = await request.post()
        username = form.get('username')
        password = form.get('password')
        verified = await ylitse.check_credentials(
            request.app['accounts'], username, password)

        if verified:
            response = web.json_response({'message': 'You were logged in.'})

            await remember(request, response, username)

            return response

        return web.HTTPUnauthorized()

    @login_required
    async def logout(self, request):
        """Process logout request."""
        response = web.json_response({'message': 'You were logged out.'})
        await forget(request, response)

        return response

    @login_required
    async def get_user(self, request):
        """Get current user."""
        account = request.app['accounts'].get(await authorized_userid(request))
        return web.json_response({**account, 'password': None})
