"""Auth module."""

import base64

import cryptography
from passlib.hash import argon2
import aiohttp_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage
import aiohttp_security
from aiohttp_security.abc import AbstractAuthorizationPolicy


class MemoryAuthorizationPolicy(AbstractAuthorizationPolicy):
    """In-memory authorization policy implementation."""

    def __init__(self, accounts):
        """Policy constructor."""
        super().__init__()

        self.accounts = accounts

    async def authorized_userid(self, identity):
        """Retrieve authorized user id."""
        if identity in self.accounts:
            return identity

    async def permits(self, identity, permission, context=None):
        """Check user permissions."""
        a = self.accounts.get(identity)

        if not a:
            return False

        return permission == a.get('role')


async def check_credentials(accounts, username, password):
    """Simply check if password matches."""
    a = accounts.get(username)

    if not a:
        return False

    return argon2.verify(password, a.get('password'))


def hash_password(password):
    """Create a password hash."""
    return argon2.hash(password)


def setup_auth(app):
    """Configure session and security."""
    fernet_key = cryptography.fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(fernet_key)

    cookie = EncryptedCookieStorage(secret_key, cookie_name='YLITSE')
    aiohttp_session.setup(app, cookie)

    policy = aiohttp_security.SessionIdentityPolicy()
    aiohttp_security.setup(
        app, policy, MemoryAuthorizationPolicy(app['accounts']))
