"""Web application module.

Provides methods for creating and configuring aiohhtp web application.
"""

import os
import time

import yaml
import jsonschema
import aiohttp

import ylitse


CONFIG_PATH = './config.yaml'
CONFIG_SCHEMA = {
    'type': 'object',
    'properties': {
        'data_file': {'type': 'string'},
        'data_file_skills': {'type': 'string'},
        'data_file_messages': {'type': 'string'},
        'admin_account': {
            'type': 'object',
            'properties': {
                'role': {'type': 'string'},
                'username': {'type': 'string'},
                'password': {'type': 'string'},
            },
        },
    },
}


def load_config(path):
    """Load and validate configuration file."""
    with open(path) as f:
        config = yaml.safe_load(f)
        jsonschema.validate(config, CONFIG_SCHEMA)

        return config


def create_app():
    """Create a web app to be passed to a server."""
    app = aiohttp.web.Application()
    app['config'] = load_config(os.environ.get('YLITSE_CONFIG', CONFIG_PATH))

    saved_accounts = ylitse.read_file_to_list(app['config']['data_file'])
    if 'admin' not in [u['username'] for u in saved_accounts]:
        admin_account = {
            'id': str(time.time()).replace('.', ''),
            'username': app['config']['admin_username'],
            'password': ylitse.hash_password(app['config']['admin_password']),
            'role': 'admin',
        }
        saved_accounts.append(admin_account)

    ylitse.write_list_to_file(saved_accounts, app['config']['data_file'])

    app['accounts'] = {a['username']: a for a in saved_accounts}
    app['skills'] = ylitse.read_file_to_list(app['config']['data_file_skills'])

    ylitse.setup_routes(app)
    ylitse.setup_middleware(app)
    ylitse.setup_auth(app)

    return app
