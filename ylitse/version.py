"""Version module.

This module contains only project version info. It is used by setup.py:

    python setup.py --version

And version endpoint among others:

    curl 127.0.0.1:8080/version

Remember to update NEWS file too when bumping a version.
"""

__version__ = '0.1.0+git'
