#!/usr/bin/python3

from setuptools import setup

import ylitse


setup(
    name='ylitse-api',
    version=ylitse.__version__,
    description='Ylitse peer mentoring service HTTP API',
    author='Futurice Chilicorn Fund',
    author_email='spice@futurice.com',
    url='https://gitlab.com/ylitse/ylitse-api',
    license='MIT',
    packages=['ylitse-api'],
)
