all: run

env:
	python3 -m venv env
	. env/bin/activate && \
	pip install -r requirements.txt

upgrade:
	. env/bin/activate && \
	pip install -Ur requirements-base.txt && \
	pip freeze > requirements.txt

lint:
	pycodestyle ylitse
	pycodestyle tests/*.py
	pylint -j0 ylitse
	pylint -j0 -d missing-docstring tests/*.py
	pydocstyle ylitse
	yamllint -c .yamllintrc tests/api

unittest:
	coverage run --branch --source=ylitse \
		-m unittest discover tests "*_tests.py"
	coverage report -m

apitest:
	gunicorn -c gunicorn.conf --check-config ylitse.main:app
	gunicorn -c gunicorn.conf ylitse.main:app > /dev/null 2>&1 &
	while ! [ -f /tmp/ylitse.pid ]; do sleep 1; done
	pyresttest http://127.0.0.1:8080 tests/api/config.yaml \
		--vars '${shell ./scripts/yaml2json config.yaml}' || true
	pkill -F /tmp/ylitse.pid && rm /tmp/ylitse.pid

test: unittest lint apitest

run-standalone:
	PYTHONPATH=. python ylitse/main.py --host 0.0.0.0 --port 8080

run-gunicorn:
	gunicorn -c gunicorn.conf --check-config ylitse.main:app
	gunicorn -c gunicorn.conf --reload ylitse.main:app

run: test run-standalone

clean:
	find . -type f -name '*~' -exec rm -f {} \;
	find . -type d -name '__pycache__' -prune -exec rm -rf {} \;
	coverage erase

.PHONY: all env upgrade lint unittest apitest test run-standalone run-gunicorn run clean
